
local lj_glfw = require "glfw"
local ffi = require "ffi"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local cstr_t       = ffi.typeof("char[?]")
local src_ptr_t    = ffi.typeof("const char*[1]")
local len_ptr_t    = ffi.typeof("int[1]")
local int_buffer_t = ffi.typeof("int[1]")

local function getLog(shader)
	local b = int_buffer_t()
	glext.glGetObjectParameterivARB(shader, glc.GL_OBJECT_INFO_LOG_LENGTH_ARB, b)
	if b[0] ~= 0 then
		local log_buffer = cstr_t(b[0])
		glext.glGetInfoLogARB(shader, b[0], b, log_buffer)
		return ffi.string(log_buffer, b[0])
	end
end

local str2enum = {
	["fragment"] = glc.GL_FRAGMENT_SHADER_ARB,
	["vertex"]   = glc.GL_VERTEX_SHADER_ARB,
	["geometry"] = glc.GL_GEOMETRY_SHADER_ARB,
}

local GLProgram = {}
GLProgram.__index = GLProgram

local function GLProgram_new()
	local program = glext.glCreateProgramObjectARB()
	assert(program ~= 0, "couldn't create glsl program")
	
	return setmetatable({
		id = program,
		linked = false,
		uniforms = {},
		attributes = {},
	}, GLProgram)
end

function GLProgram:addShader(typ, src)
	assert(not self.linked, "tried to add shader after program linked")
	local shader = glext.glCreateShaderObjectARB(str2enum[typ] or typ)
	assert(shader ~= 0, "couldn't create glsl shader")
	
	local src_arr = src_ptr_t()
	src_arr[0] = src
	local len_arr = len_ptr_t(#src)
	local b = int_buffer_t()
	
	glext.glShaderSourceARB(shader, 1, src_arr, len_arr)
	glext.glCompileShaderARB(shader)
	
	-- Check errors
	local err, compilermsg
	glext.glGetObjectParameterivARB(shader, glc.GL_OBJECT_COMPILE_STATUS_ARB, b)
	err = b[0] == 0
	
	-- Check log
	local compilermsg = getLog(shader)
	
	if err then
		error(compilermsg, 0)
	else
		glext.glAttachObjectARB(self.id, shader)
		glext.glDeleteObjectARB(shader)
		--lujgl.checkError()
		return compilermsg
	end
end

function GLProgram:link()
	assert(not self.linked, "tried to link multiple times")
	glext.glLinkProgramARB(self.id)
	
	-- Check for errors
	local b = int_buffer_t()
	local err, compilermsg
	glext.glGetObjectParameterivARB(self.id, glc.GL_OBJECT_LINK_STATUS_ARB, b)
	err = b[0] == 0
	
	-- Check log
	local compilermsg = getLog(self.id)
	
	glext.glValidateProgramARB(self.id)
	compilermsg = compilermsg and compilermsg..(getLog(self.id) or "") or getLog(self.id)
	
	if err then
		error(compilermsg, 0)
	end
	
	self.linked = true
	return compilermsg
end

function GLProgram:getUniformLocation(name)
	if self.uniforms[name] then
		return self.uniforms[name]
	else
		local id = glext.glGetUniformLocationARB(self.id, name)
		--lujgl.checkError()
		self.uniforms[name] = id
		return id
	end
end

function GLProgram:getAttribLocation(name)
	if self.attributes[name] then
		return self.attributes[name]
	else
		local id = glext.glGetAttribLocationARB(self.id, name)
		self.attributes[name] = id
		return id
	end
end

function GLProgram:use()
	assert(self.linked, "tried to use unlinked glsl program")
	glext.glUseProgramObjectARB(self.id)
end

function GLProgram:delete()
	glext.glDeleteObjectARB(self.id)
	self.id = 0
end

return GLProgram_new
