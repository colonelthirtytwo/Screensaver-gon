
local lj_glfw = require "glfw"
local ffi = require "ffi"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()
math.randomseed(os.time())

lj_glfw.init()

local window, monitormode

do
	local monitor = lj_glfw.getPrimaryMonitor()
	monitormode = glfw.glfwGetVideoMode(monitor)
	
	window = lj_glfw.Window(monitormode.width, monitormode.height, "Super Hexagon?")
end

window:makeContextCurrent()

local GLProgram = require "shader"

local vec4 = ffi.typeof "double[4]"

-- ----------------------------------------------------------------------------------------------------

local TICK_LEN = 1/120
local CENTER_HEXAGON_SIZE = 0.09
local CENTER_HEXAGON_WIDTH = 0.005

-- ----------------------------------------------------------------------------------------------------

local Wall = ffi.typeof[[struct {
	int column;
	double len;
	double r;
}]]

do
	local Wall_mt = {}
	Wall_mt.__index = Wall_mt
	
	function Wall_mt:__tostring()
		return string.format("Wall(%d, %f, %f)", self.column, self.len, self.r)
	end
	
	ffi.metatype(Wall, Wall_mt)
end

-- ----------------------------------------------------------------------------------------------------

local stage = {
	bgcolor1 = vec4(0.5, 0.5, 0.0, 0.9),
	bgcolor2 = vec4(0.2, 0.2, 0.0, 0.9),
	fgcolor  = vec4(0.9, 0.9, 0.0, 1.0),
	wallspeed = 0.5,
	rotspeed = 1,
	spacing = 0.5,
	patterns = {},
	
	patterntotal = 0,
	currot = math.random()*2*math.pi,
	height = 0,
	
	walls = {},
}

function stage:addWall(w)
	table.insert(self.walls, w)
	self.height = math.max(self.height, w.r + w.len)
end

function stage:addPattern(arr)
	arr.weight = arr.weight or 1
	table.insert(self.patterns, arr)
	self.patterntotal = self.patterntotal + arr.weight
end

function stage:getRandomPattern()
	local r = math.random()*self.patterntotal
	for i=1,#self.patterns do
		local p = self.patterns[i]
		if p.weight > r then
			return p
		end
		r = r - p.weight
	end
	assert(false)
end

function stage:spawnPattern(pattern, r, rot)
	for i=1,#pattern do
		local template = pattern[i]
		self:addWall(Wall(
			(template.column + rot) % 6,
			template.len,
			template.r + r
		))
	end
end

-- ----------------------------------------------------------------------------------------------------

-- Three spokes
stage:addPattern {
	Wall(0, 0.08, 0),
	Wall(2, 0.08, 0),
	Wall(4, 0.08, 0),
	
	weight = 1,
}

-- Single C
stage:addPattern {
	Wall(0, 0.08, 0),
	Wall(1, 0.08, 0),
	Wall(2, 0.08, 0),
	Wall(3, 0.08, 0),
	Wall(4, 0.08, 0),
	
	weight = 1.0,
}

-- Triple C
stage:addPattern {
	Wall(0, 0.08, 0.0),
	Wall(1, 0.08, 0.0),
	Wall(2, 0.08, 0.0),
	Wall(3, 0.08, 0.0),
	Wall(4, 0.08, 0.0),
	
	Wall(0, 0.08, 0.4),
	Wall(1, 0.08, 0.4),
	Wall(3, 0.08, 0.4),
	Wall(4, 0.08, 0.4),
	Wall(5, 0.08, 0.4),
	
	Wall(0, 0.08, 0.8),
	Wall(1, 0.08, 0.8),
	Wall(2, 0.08, 0.8),
	Wall(3, 0.08, 0.8),
	Wall(4, 0.08, 0.8),
	
	weight = 0.5,
}

-- Left+Right
stage:addPattern {
	Wall(0, 1.08, 0.0),

	Wall(2, 0.08, 0.0),
	Wall(3, 0.08, 0.0),
	Wall(4, 0.08, 0.0),
	Wall(5, 0.08, 0.0),
	
	Wall(1, 0.08, 0.5),
	Wall(2, 0.08, 0.5),
	Wall(3, 0.08, 0.5),
	Wall(4, 0.08, 0.5),
	
	Wall(2, 0.08, 1.0),
	Wall(3, 0.08, 1.0),
	Wall(4, 0.08, 1.0),
	Wall(5, 0.08, 1.0),
	
	weight = 0.3,
}

-- Latter
stage:addPattern {
	Wall(0, 1.28, 0.0),
	Wall(3, 1.28, 0.0),
	
	Wall(1, 0.08, 0.0),
	Wall(4, 0.08, 0.0),
	
	Wall(2, 0.08, 0.3),
	Wall(5, 0.08, 0.3),
	
	Wall(1, 0.08, 0.6),
	Wall(4, 0.08, 0.6),
	
	Wall(2, 0.08, 0.9),
	Wall(5, 0.08, 0.9),
	
	Wall(1, 0.08, 1.2),
	Wall(4, 0.08, 1.2),
	
	weight = 0.3,
}

-- ----------------------------------------------------------------------------------------------------

local hexagon_program
do
	local vertex_source = [[
		#version 120
		uniform float rotation = 0.0;
		uniform float pulse = 1.0;
		
		const float one_sixth_rotation = 2.0 * 3.1415926535897932384626433832795 / 6.0;
		
		void main()
		{
			vec2 pos = vec2(
				cos(gl_Vertex.x*one_sixth_rotation + rotation),
				sin(gl_Vertex.x*one_sixth_rotation + rotation)
			) * gl_Vertex.y * pulse;
			
			gl_Position = gl_ModelViewProjectionMatrix * vec4(pos, 0, 1);
			gl_FrontColor = gl_Color;
		}
	]]
	
	local frag_source = [[
		#version 120
		void main()
		{
			gl_FragColor = gl_Color;
		}
	]]
	
	hexagon_program = GLProgram()
	hexagon_program:addShader("vertex", vertex_source)
	hexagon_program:addShader("fragment", frag_source)
	hexagon_program:link()
	
	hexagon_program:use()
	hexagon_program:getUniformLocation("rotation")
	hexagon_program:getUniformLocation("pulse")
	glext.glUseProgramObjectARB(0)
	
	gl.glMatrixMode(glc.GL_PROJECTION)
	gl.glLoadIdentity()
	
	local w, h = window:getFramebufferSize()
	local aspect = (w/h)/2
	gl.glOrtho(-aspect, aspect, -0.5, 0.5,-1,1)
	gl.glMatrixMode(glc.GL_MODELVIEW)
	gl.glLoadIdentity()
	gl.glDisable(glc.GL_DEPTH_TEST)
	gl.glEnable(glc.GL_BLEND)
	gl.glBlendFunc(glc.GL_SRC_ALPHA, glc.GL_ONE_MINUS_SRC_ALPHA)
	gl.glClearColor(0,0,0,0)
end

-- ----------------------------------------------------------------------------------------------------

do
	ffi.cdef[[
		void* glfwGetWin32Window (GLFWwindow *window);
		long GetWindowLongW(void*, int);
		long SetWindowLongW(void*, int, long);
		int SetWindowPos(void*, void*, int, int, int, int, unsigned int);
		
		void* CreateRectRgn(int, int, int, int);
		
		typedef struct {
			unsigned long dwFlags;
			int  fEnable;
			void*  hRgnBlur;
			int  fTransitionOnMaximized;
		} DWM_BLURBEHIND;
		
		long DwmEnableBlurBehindWindow(void*, const DWM_BLURBEHIND*);
	]]
	local hWnd = lj_glfw.glfw.glfwGetWin32Window(window)
	
	local dwm = ffi.load("dwmapi")
	local u32 = ffi.load("User32")
	
	local style = u32.GetWindowLongW(hWnd, -16)
	style = bit.band(style, bit.bnot(bit.bor(0, 0x00C00000, 0x00080000, 0x00040000, 0x00020000, 0x00010000)))
	style = bit.bor(style, 0x80000000)
	u32.SetWindowLongW(hWnd, -16, style)
	
	local rect = ffi.C.CreateRectRgn(0,0,1,1)
	
	local bb = ffi.new("DWM_BLURBEHIND")
	bb.dwFlags = bit.bor( 0x00000001, 0x00000002)
	bb.fEnable = 1
	bb.hRgnBlur = rect
	dwm.DwmEnableBlurBehindWindow(hWnd, bb)
	
	u32.SetWindowPos(hWnd, ffi.cast("void*", -1), 0, -30, monitormode.width, monitormode.height+20, 0)
	
	gl.glClearColor(0,0,0,0)
end

-- ----------------------------------------------------------------------------------------------------

local lastt = lj_glfw.getTime()
local function think()
	local t = lj_glfw.getTime()
	local ticks = math.floor((t - lastt)/TICK_LEN)
	
	for i=1,ticks do
		-- Change stage
		stage.currot = stage.currot + stage.rotspeed * TICK_LEN
		
		-- Move walls down
		for i, wall in pairs(stage.walls) do
			wall.r = wall.r - stage.wallspeed*TICK_LEN
			if wall.r + wall.len < 0 then
				stage.walls[i] = nil
			end
		end
		stage.height = stage.height - stage.wallspeed*TICK_LEN
		
		-- Add new patterns
		while stage.height < 4 do
			local r = math.max(stage.height, 0) + stage.spacing
			local rot = math.random(0,5)
			local pattern = stage:getRandomPattern()
			stage:spawnPattern(pattern, r, rot)
		end
	end
	
	lastt = lastt + ticks*TICK_LEN
end

local function draw()
	gl.glClear(glc.GL_COLOR_BUFFER_BIT)
	gl.glEnable(glc.GL_BLEND)
	hexagon_program:use()
	
	glext.glUniform1fARB(hexagon_program:getUniformLocation("rotation"), stage.currot)
	--glext.glUniform1fARB(hexagon_program:getUniformLocation("pulse"), 1+math.sin(lujgl.getTime()*7)*0.1)
	
	-- Background
	gl.glColor4dv(stage.bgcolor1)
	gl.glBegin(glc.GL_QUADS)
	for i=0,5,2 do
		gl.glVertex2d(i  ,CENTER_HEXAGON_SIZE)
		gl.glVertex2d(i  ,CENTER_HEXAGON_SIZE+100)
		gl.glVertex2d(i+1,CENTER_HEXAGON_SIZE+100)
		gl.glVertex2d(i+1,CENTER_HEXAGON_SIZE)
	end
	gl.glEnd()
	
	gl.glColor4dv(stage.bgcolor2)
	gl.glBegin(glc.GL_QUADS)
	for i=1,6,2 do
		gl.glVertex2d(i  ,CENTER_HEXAGON_SIZE)
		gl.glVertex2d(i  ,CENTER_HEXAGON_SIZE+100)
		gl.glVertex2d(i+1,CENTER_HEXAGON_SIZE+100)
		gl.glVertex2d(i+1,CENTER_HEXAGON_SIZE)
	end
	gl.glEnd()
	
	gl.glBegin(glc.GL_TRIANGLE_FAN)
	gl.glVertex2d(0,0)
	for i=0,6 do
		gl.glVertex2d(i,CENTER_HEXAGON_SIZE)
	end
	gl.glEnd()
	
	-- Center hexagon
	gl.glColor4dv(stage.fgcolor)
	gl.glBegin(glc.GL_TRIANGLE_STRIP)
	for i=0,6 do
		gl.glVertex2d(i,CENTER_HEXAGON_SIZE-CENTER_HEXAGON_WIDTH)
		gl.glVertex2d(i,CENTER_HEXAGON_SIZE)
	end
	gl.glEnd()
	
	-- Walls
	for i, wall in pairs(stage.walls) do
		gl.glBegin(glc.GL_TRIANGLE_STRIP)
		
		gl.glVertex2d(wall.column  ,CENTER_HEXAGON_SIZE+math.max(0, wall.r))
		gl.glVertex2d(wall.column  ,CENTER_HEXAGON_SIZE+wall.r+wall.len)
		gl.glVertex2d(wall.column+1,CENTER_HEXAGON_SIZE+math.max(0, wall.r))
		gl.glVertex2d(wall.column+1,CENTER_HEXAGON_SIZE+wall.r+wall.len)
		
		gl.glEnd()
	end
	
	glext.glUseProgramObjectARB(0)
end

while not window:shouldClose() do
	think()
	draw()
	
	window:swapBuffers()
	lj_glfw.pollEvents()
end

window:destroy()
